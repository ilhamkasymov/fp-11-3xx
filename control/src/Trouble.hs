{-==== Касымов Ильхам, 11-302 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}

 rocket :: [(Integer, Integer)] -> [(Integer, Integer)]

rocket xs
  | snd maxPair == findMin xs (snd (head xs)) = [maxPair]
  | otherwise = xs
  where maxPair = findMaxPair xs (head xs)

findMaxPair :: [(Integer, Integer)] -> (Integer, Integer) -> (Integer, Integer)
findMaxPair ((first, second) : []) (maxFirst, maxSecond)
  | first `div` second > maxFirst `div` maxSecond = (first, second)
  | first `div` second == maxFirst `div` maxSecond && second <= maxSecond = (first, second)
  | otherwise = (maxFirst, maxSecond)
findMaxPair ((first, second) : tail) (maxFirst, maxSecond)
  | first `div` second > maxFirst `div` maxSecond = findMaxPair tail (first, second)
  | first `div` second == maxFirst `div` maxSecond && second <= maxSecond = findMaxPair tail (first, second)
  | otherwise = findMaxPair tail (maxFirst, maxSecond)

findMin :: [(Integer, Integer)] -> Integer -> Integer
findMin list minValue = foldl (\x (first, second) -> min x second) minValue list

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
 orbiters :: [Spaceship a] -> [Load a]

orbiters [] = []
orbiters list = concatMap oneCargo list

oneCargo :: Spaceship a -> [Load a]
oneCargo (Cargo cargo) = filter isOrbiter cargo
    where isOrbiter (Orbiter _) = True
          isOrbiter _ = False
oneCargo (Rocket _ list) = orbiters list

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
 finalFrontier [] = []
finalFrontier (head : list) = case head of
  Warp _ -> ["Kirk"] ++ finalFrontier(list)
  BeamUp _ -> ["Kirk"] ++ finalFrontier(list)
  IsDead _ -> ["McCoy"] ++ finalFrontier(list)
  LiveLongAndProsper -> ["Spock"] ++ finalFrontier(list)
  Fascinating -> ["Spock"] ++ finalFrontier(list)

