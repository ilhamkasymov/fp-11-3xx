


task_18 = print (head $ foldr1 g tri) 
  where
    f x y z = x + max y z
    g xs ys = zipWith3 f xs ys $ tail ys
    tri = [
        [12],
        [43,57],
        [11,33,89],
        [61,25,44,19],
        [77,02,65,99,13],
        [19,18,22,39,51,80],
        [88,26,96,75,74,60,47],
        [99,63,05,38,46,14,78,32],
        [40,61,16,56,84,10,80,44,17],
        [37,28,86,19,74,93,58,32,94,39],
        [85,11,39,76,34,04,88,55,92,33,12],
        [70,11,33,28,77,73,17,78,39,68,17,57],
        [91,71,52,38,17,14,91,43,58,50,27,29,48],
        [63,66,04,68,89,53,67,30,73,16,69,87,40,31],
        [04,62,98,27,23,09,70,98,73,93,38,53,60,04,23]]
