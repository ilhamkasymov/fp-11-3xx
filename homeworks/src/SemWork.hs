module SemWork where

import Data.Char

data Term = Var String
          | Lam String Term  
          | Apply Term Term
          deriving Eq

instance Show Term where
    show (Var v) = v
    show (Lam v t) = "(\\" ++ v ++ " . " ++ show t ++ ")"
    show (Apply t1 t2) = show t1 ++ " " ++ show t2

--Вычисление терма на один шаг
evaluate :: Term -> Maybe Term
evaluate term@(Apply apply@(Apply t1 t2) t3) = Just $ Apply evaluate_apply t3
    where (Just evaluate_apply) = evaluate apply
evaluate term@(Apply t1 apply@(Apply t2 t3)) = Just $ Apply t1 evaluate_apply
    where (Just evaluate_apply) = evaluate apply
evaluate term@(Apply lam1@(Lam v1 t1) l2) = Just (swap v1 l2 t1)
evaluate term = Just term

-- замена переменной var на term в inTerm для разных случаев
swap :: String -> Term -> Term -> Term
swap var term inTerm = case inTerm of
    Var v -> if v == var 
                    then term 
                    else Var v
    lam@(Lam v (Var v1)) -> if v /= v1 && v1 == var 
                    then (                 
                        if Var v == term 
                        then Lam (new_val v) term  
                        else Lam v term )
                    else lam 
    lam@(Lam v (Lam v2 t)) -> if v == var || v2 == var
                    then lam
                    else if term == Var v
                    then  Lam (new_val v) (Lam v2 (swap var term (swap v (Var $ new_val v) t )))
                    else if term == Var v2 
                    then Lam v (Lam (new_val v2) (swap var term (swap v2 (Var $ new_val v2) t )))
                    else Lam v (Lam v2 (swap var term t))
    lam@(Lam v (Apply t1 t2)) -> if v == var
                    then lam
                    else if term == Var v 
                        then Lam (new_val v) (swap var term (swap v (Var $ new_val v) (Apply t1 t2)))
                        else Lam v (swap var term (Apply t1 t2))
    apply@(Apply t1 t2) -> Apply (swap var term t1) (swap var term t2)
    where new_val v = (swapToFreeVariable [v] "a")
          
-- Получиение всех используемых переменных из term
getRelatedVariables :: Term -> [String]
getRelatedVariables term = case term of
    Lam v t -> v : getRelatedVariables t
    Apply t1 t2 -> getRelatedVariables t1 ++ getRelatedVariables t2
    Var v -> [v]
-- замена аргумента на свободную переменную (не используемую букву), не входящуюю в список getRelatedVariables 
swapToFreeVariable :: [String] -> String -> String
swapToFreeVariable getRelatedVariables var = if var `elem` getRelatedVariables
                            then swapToFreeVariable getRelatedVariables (nextVar var)
                            else var
                 where nextVar [ch] = [chr (ord ch + 1)]

-- Вычисление на много шагов
evaluate' :: Term -> Term
evaluate' term@(Apply apply@(Apply t1 t2) t3) -> evaluate' $ Apply (evaluate' apply) t3
evaluate' term@(Apply t1 apply@(Apply t2 t3)) -> evaluate' $ Apply t1 (evaluate' apply)
evaluate' term@(Apply lam1@(Lam v1 t1) l2) -> evaluate' $ swap v1 l2 t1
evaluate' term -> term